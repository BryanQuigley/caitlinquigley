<!--
.. title: SERVICES
.. slug: services
.. date: 2017-12-24 02:37:42 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Services include:

**Communications & Marketing** *writing, editing, storytelling, strategy*

**Project Management** *cheerful stewardship of teams*

**Design** *design and layout for digital and print*

**Research** *interviews, qualitative data analysis*


Clients include:

* Bread & Roses Community Fund
* Cooperative Economics Alliance of New York City
* Gender Justice Fund
* Grounded Solutions Network
* Haverford College
* Paul Robeson House & Museum
* Philadelphia Area Cooperative Alliance
* Philadelphia Museum of Art
* Philadelphia Reads
* Praxis Consulting Group
* Schumacher Center for a New Economics
* Slow Rise Bakery
* Sunday Suppers
* U.S. Federation of Worker Cooperatives
* Walnut Hill Community Association

Contact Caitlin on [LinkedIn](https://www.linkedin.com/in/caitlinquigley/).

