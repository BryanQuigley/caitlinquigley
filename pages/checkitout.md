<!--
.. title: CHECK IT OUT
.. slug: checkitout
.. date: 2017-12-24 02:37:42 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Here are some things I've made that I invite you to check out.

[Models for community control of land: a poster set for popular education](https://drive.google.com/drive/folders/1kh-d67Mdt1eKTCHUQfeZsYLORvkgsW0n?usp=sharing)
<br>This set of posters is intended to be used at a gathering of people who want to learn how other communities have tried to address issues like gentrification, displacement, and disinvestment. The models featured in the posters emphasize two things: long-term solutions for affordability and democratic, community-based control. 

[An Amazing Opportunity for You (to Avoid)!](https://indd.adobe.com/view/311567a2-7e0d-4762-b807-703f506dee9c) (16 little pages)
<br> I created this zine about multi-level marketing in early 2019.
