<!--
.. title: ABOUT
.. slug: index
.. date: 2017-12-24 02:48:11 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->



<img style="float: left; padding: 0px 15px 15px 0px;" src="images/Caitlin_Quigley_photo_by_Kyle_Cassidy.jpg" hspace="0">

Caitlin Quigley does consulting work for social change organizations, cooperatives, and small businesses.

Most recently, Caitlin served as director of communications & development at Bread & Roses Community Fund, where she moved money to movements for change in the Philadelphia region.

Caitlin co-founded the Philadelphia Area Cooperative Alliance (PACA), a 501(c)3 cooperative development center and chamber of commerce for cooperatives and credit unions. In 2016, Caitlin was named a winner of the Knight Cities Challenge for her project 20 Book Clubs → 20 Cooperative Businesses, which organized 185 people to study economic cooperation and then form their own cooperative businesses.

Caitlin was part of the Leadership Philadelphia Connectors and Keepers program in 2018 and she completed the Philadelphia Citizens Planning Institute in 2015. She has a Certificate in Nonprofit Marketing and Communications from the Nonprofit Center at LaSalle University and a Certificate in Digital Media for Social Impact from the Center for Social Impact Strategy at the University of Pennsylvania.

Caitlin holds a bachelor’s degree from Wesleyan University and a master's degree in social justice and community organizing from Prescott College.

<i> Photo by Kyle Cassidy </i>
